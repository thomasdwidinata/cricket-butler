# Cricket Butler


## Introduction

During my high school studies, I was introduced into the world of robotics. During that time, robotics are seen as the future of automation, computing, and industry. Joining the robotics club sure would challenge me into not only knowing how to run a robot, but goes deep into the world of programming through my university life and career.

That time, the robotic club was guided by school partner's tutor, [SARI Teknologi](https://sariteknologi.com/). They use a board called i-BOX which was made by [INEX](https://inex.co.th/). The board can be programmed with a special software called **Logo Blocks**. It was an IDE for this specific board with almost identical looking programming blocks to [Scratch](https://scratch.mit.edu/), but for robot. The language itself are pretty straight forward such as motor control, sensors reading, digital input, serial communications, basic arithmetics, and basic logic.

While compiling the blocks, the IDE will convert all blocks into a custom **Cricket Logo** language and ready to be uploaded to the board. The Cricket Logo is more likely similar to a pseudo code, mostly self explanatory. You can actually directly program in Cricket Logo software and use the Cricket Logo language instead of placing blocks on Logo Blocks which has a canvas limit.

![Screenshot of Logo Blocks software](./img/logoblocks.jpg)

> There are not much information about i-BOX III nowdays as it is a discontinued product that circles around 2013 around my school. But the manufacturer of the board do have slight information and software download center too. Head on to [INEX Global](https://www.inexglobal.com/products.php?pcode=8013001&type=micro) website to download the software.

After digging some more information, apparently this board almost identical and featured on UMASS Lowell with same software and capabilities. It is called [Handy Cricket](https://www.cs.uml.edu/~fredm/handyboard.com/cricket/index.html)

It seems that there are a lot of variants and derivatives of the Handy Cricket board and i-BOX board. My board variant itself are called **i-BOX III** with specification below:
- Power : 4xAA cell battery (4.8v), tested 5v capable
- CPU : [MB89F202](https://datasheetspdf.com/pdf-file/504499/Fujitsu/MB89F202/1)
    - 16K x 8bits ROM size
    - 512 x 8bits RAM size
- EEPROM : [24LC128](https://www.microchip.com/en-us/product/24lc128)
- 4x Digital Input
- 4x Analog Input
- 4x [HT6751B Motor Driver](https://www.digchip.com/datasheets/download_datasheet.php?id=385479&part-number=HT6751B) (With total 8 ports: 4 forward, 4 reverse)
- 2x Communication Port
- 1x Piezo Speaker
- 1x Start/stop button
- 1x RJ9 (What seems to be, not sure) for Serial communication

## The Goal

Since the The goal of this project is to make this board as a "co-processor" for other boards, current source code on this project is only to program the board to receive commands via serial communication and acts pre-defined functions. The IDE itself do have a function called **Command Center** which lets you send Cricket Logo syntax and immediately execute the command. Currently, it will only have functionalities of using motor driver, receive sensor input, and interact with communication P0 and P1 on the board.

## Getting Started

To get started with the board, you need a serial connection to the board. I use the provided cable which is called UCON-200 USB to Serial with a receptacle that seems to be an RJ9 connector. This will allow to upload program and receive serial input via Serial communication.

Then you need to download either **Logo Blocks** or **Cricket Logo**, either from INEX Global website or Handy Cricket.

## Uploading the Program

There are currently 2 source code on this project, both files named `v1`. Depending on the software you use, make sure to open the appropriate file type. Such that Logo Blocks will only accept `.lbk` file, while Cricket Logo will only accept `.lgo`.

> Due to limitation of the software, try not to change the extension. Otherwise it won't read the source code even if there is not syntax error.

Select the serial port of your board, then click `Download` to upload the code into the board.

## Usage on Other Boards

To communicate with the board, below are the default configuration of the board:
- Baud Rate : 9600
- Parity : No parity
- Data Bits : 8
- Stop Bit : 1
- Hand Shake : No Hand Shaking

The board only accepts 1 character at a time (According to my source code for now). It accept only ASCII character and it will be converted into decimal to be decoded based on the source code. No need to enter end line character as it immediately execute the desired function.

The board will have 2 variables, to store numerical string (`0` - `9`) and alphabet (Non case sensitive). The alphabet will trigger a function while the numerical will be affecting the function. Below are the documentation:

#### Functions
- A : Motor ON
- B : Motor OFF
- C : Motor Brake
- D : Motor This Way
- E : Motor That Way
- F : Motor Set Power
- G : Beep how many
- H : Get Sensor
- I : Set P1 P0 High/Low

#### Numerical Values
##### Number parameter for function F to I
- F : To set the motor power. Accepts `1` to `9`
- G : To produce beep how many times. Accepts `1` to `9`
- H : Specify which sensor to read. Accepts `0` to `3`
- I : Specify which communication port. Accepts `0` and `1`

##### Usage for function A to E
- 0 : Motor a
- 1 : Motor b
- 2 : Motor c
- 3 : Motor d
- 4 : Motor ab
- 5 : Motor cd
- 6 : Motor abcd

> Documentation needs to be fixed. WIP

## Example Serial Command

- `1AD8F`
    - `1` prepare to select Motor A (As the next command is `A`)
    - `A` Motor ON - based from previous numerical value given, `1` means Motor A. Triggering `A` function with `1` means turning on Motor A
    - `D` Motor This Way - Trigger function `D` and sets currently running motor to turn clockwise
    - `8` prepare to select 8 as motor power (As the next command is `F`)
    - `F` Tell motor driver to set power to `8` (Max) to currently active motor

## License
This project is licensed under the [MIT license](./LICENSE).

## Project status
The **Logo Blocks** software provides a feature called **Command Center**, which allow Cricket Logo syntax to be immediately send and executed by the board itself. The next step of this project is to implement the Command Center to create a custom library that can be used by developers to easily access this board without having to remember very syntax on this project. My intention is to create an Arduino library or Python library (Specifically for Raspberry Pi) so this i-BOX III board can be used as "co-processor".